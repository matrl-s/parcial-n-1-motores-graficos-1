using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyControler : MonoBehaviour
{
    public Transform player;
    private int hp;
    public int speed;
    float persecuitDistance = 10f;
    float distance;
    

    private void Start()
    {
        hp = 5;
    }

    private void Update()
    {
        Vector3 posNoRot = new Vector3(player.position.x, 2.1f, player.position.z);
        transform.LookAt(posNoRot);
        distance = Vector3.Distance(transform.position, player.position);
        if (distance < persecuitDistance)
        {
            transform.Translate(speed * Vector3.forward * Time.deltaTime);
        }

        if(hp <= 0)
        {
            Destroy(gameObject);
            GameManager.totalKills--;
        }

    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            Debug.Log("HE RECIBIO DA�O");
            hp = hp - 10;
        }
    }
}
