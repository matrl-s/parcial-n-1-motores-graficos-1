using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropellerControler : MonoBehaviour
{
    public Transform propellerLeft;
    public Transform propellerRight;

    void Update()
    {
        propellerRight.transform.Rotate(3, 0, 0);
        propellerLeft.transform.Rotate(3, 0, 0);
    }
}
