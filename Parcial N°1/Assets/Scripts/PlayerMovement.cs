using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Transform cam; //Posicion de la camara.
    float hMouse; 
    float vMouse; 
    float yReal = 0.0f; //Rotacion de camara inicial y es lo que va a ir variando.
    public float horizontalSpeed;
    public float verticalSpeed; 
    //Movement
    public CharacterController controller; //Controlar el personaje.
    public float speed = 12f; //Velocidad del personaje.
    float x; 
    float z; 
    Vector3 move; // Vector para asignar el movimiento.
    //Gravity
    Vector3 velocity; //Controla la gravedad de caida.
    public float gravity = -15f;
    bool isGrounded = false; //Controlador si esta en el piso.
    //Jump
    public float jumpForce = 1f;
    float jumpValue;
    int currentJump = 0;
    int maxJump = 2;
    //PowerUp
    float c;
    float t;
    float v;
    public GameObject powerUp;

     void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        jumpValue = Mathf.Sqrt(jumpForce * -2 * gravity); //Ecuacion del salto, dandole el doble de fuerza que la gravedad.
    }

     void Update()
    {
        LookMouse();

        if (isGrounded == true && velocity.y < 0) //Cada vez que toca el suelo se reinicia la gravedad.
        {
            velocity.y = gravity;
        }

        Movement();
        Jump();
        velocity.y += gravity * Time.deltaTime; //Control de caida.
        controller.Move(velocity * Time.deltaTime); //Control de movimiento en el eje y.
    }

     void LookMouse() //Moviminetos del Mouse Horizontales y Verticales
    {
        hMouse = Input.GetAxis("Mouse X") * horizontalSpeed * Time.deltaTime;
        vMouse = Input.GetAxis("Mouse Y") * verticalSpeed * Time.deltaTime;

        yReal -= vMouse; //Se resta para que al mover el Mouse hacia arriba la camara gire hacia el mismo lado del mouse.
        yReal = Mathf.Clamp(yReal, -90f, 90f); //Limitar rotacion en el eje y.
        transform.Rotate(0f, hMouse, 0f);
        cam.localRotation = Quaternion.Euler(yReal, 0f, 0f); //Control de rotacion de camara
    }

     void Movement() //Movimientos adelante, atras, izquierda y derecha.
    {
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");

        move = transform.right * x + transform.forward * z; //Asignacion de moviminetos.
        controller.Move(move * speed * Time.deltaTime); //Agregado de velocidad.
    }

     void Jump() //Salto y salto doble.
    {
        if (Input.GetButtonDown("Jump") && (isGrounded || currentJump < maxJump))
        {

            isGrounded = false;
            velocity.y = jumpValue;
            currentJump++;           
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit) //Controla si esta en el piso.
    {
        if (hit.collider.CompareTag("floor"))
        {
            isGrounded = true;
            currentJump = 0;
        }

        if (hit.collider.CompareTag("powerup"))
        {
            speed = 24;
            c = 2;
            t = 2;
            v = 2;

            transform.localScale = new Vector3(c, t, v);

            Destroy(powerUp);
        }
    }
}
