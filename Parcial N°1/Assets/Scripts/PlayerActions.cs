using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerActions : MonoBehaviour
{
    public Camera posWeapon;
    public GameObject bullet;
    bool isKilled = false; //Condicion de muerte.

    private void Update()
    { 
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = posWeapon.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            GameObject pro;
            pro = Instantiate(bullet, ray.origin, transform.rotation);
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(posWeapon.transform.forward * 25, ForceMode.Impulse);
            Destroy(pro, 5);
        }
        Restart();
    }

    void Restart() //Reseteo del juego.
    {
        if (isKilled)
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
    }
    public void OnControllerColliderHit(ControllerColliderHit hit) //Compara tags para confirmar si muere el jugador.
    {
        if (hit.collider.CompareTag("Water"))
        {
            isKilled = true;
        }

    }
}
