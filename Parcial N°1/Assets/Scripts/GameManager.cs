using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    float gameTime = 60f;
    public Text txtTimer;
    public Text txtGameOver;
    public GameObject gameOverEffect;
    public Text txtTotalEnemiesKilled;
    public static int totalKills;
    public List<GameObject> enemyList;
    public Text txtWin;
    public GameObject winEffect;
    public GameObject aim;

    private void Start()
    {
        totalKills = enemyList.Count;
    }

    private void Update()
    {
        Restart();
        gameTime -= Time.deltaTime; //Contador de tiempo.
        txtTimer.text = "TIME: " + gameTime.ToString("n2"); //Texto del Tiempoo
        if(gameTime <= 0) //Condicion de GameOver.
        {
            gameOverEffect.SetActive(true); //Se activa efecto de pantalla roja.
            txtGameOver.text = "GAME OVER"; //Texto GameOver.
            Time.timeScale = 0; //Pone el juego en pausa.
            aim.SetActive(false);
        }

        txtTotalEnemiesKilled.text = "TOTAL ENEMIES: " + totalKills.ToString(); //Texto de enemigos restantes.

        if(totalKills == 0)
        {
            winEffect.SetActive(true);
            txtWin.text = "WIIIIIIIIIIIIIIIIIIIIIIIIIIN";
            Time.timeScale = 0;
            aim.SetActive(false);
        }
    }

    public void Restart() //Reseteo del juego.
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);

        }
    }
}
