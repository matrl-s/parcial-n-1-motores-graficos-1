using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyJumpControler : MonoBehaviour
{
    bool tengoQueBajar = false;
    public int speedJump = 1;
    void Update()
    {
        if (transform.position.y >= 3.2f)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <=2.03f)
        {
            tengoQueBajar = false;
        }
        if (tengoQueBajar)
        {
            GoDown();
        }
        else
        {
            GoUp();
        }

    }
    void GoUp()
    { 
        transform.position += transform.up * speedJump * Time.deltaTime;
    }
    void GoDown()
    {
        transform.position -= transform.up * speedJump * Time.deltaTime;
    }
}
